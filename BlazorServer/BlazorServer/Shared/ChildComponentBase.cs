﻿using BlazorServer.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorServer.Shared
{
    public class ChildComponentBase : ComponentBase
    {

        [Inject]
        protected RandomService RandomService { get; set; }

        protected bool DarkThemeOn;
        protected string AlertTheme => DarkThemeOn ? "dark" : "light";

        [Parameter]
        public RenderFragment ChildContent { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            DarkThemeOn = true;
        }
    }
}
