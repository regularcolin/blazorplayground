﻿using BlazorServer.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorServer.Data
{
    public class JobApplication
    {
        [Required]
        public string Fullname { get; set; }

        [StringLength(500, ErrorMessage = "Please keep it to 500 characters or less.")]
        public string Description { get; set; }

        [Required]
        [Range(10000, 100000, ErrorMessage = "Salary expectation must be from 10k-100k")]
        public int SalaryExpectation { get; set; }

        [Required]
        public bool DoesOpenSoure { get; set; }

        [Required]
        public DateTime Availability { get; set; }
    }
}
