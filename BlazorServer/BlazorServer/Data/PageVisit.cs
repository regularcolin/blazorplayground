﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorServer.Data
{
    public class PageVisit
    {
        public string Url { get; set; }
        public DateTime DateTime { get; set; }
    }
}
